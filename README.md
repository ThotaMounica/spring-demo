http://www.cronmaker.com/?1

http://localhost:8080/api/job/start/First Job

http://localhost:8080/api/job/start/Second Job


Pass the FilePath as one of the jobParameters
run=three a=b inputFile=C:\\Users\\thota\\Downloads\\SpringBatch\\spring-batch\\InputFiles\\students.csv
inputFile=C:\\Users\\thota\\Downloads\\SpringBatch\\spring-batch\\InputFiles\\students.json

inputFile=C:\\Users\\thota\\Downloads\\SpringBatch\\spring-batch\\InputFiles\\students.xml
outputFile=C:\\Users\\thota\\Downloads\\SpringBatch\\spring-batch\\OutputFiles\\studentsOutput.csv
outputFile=C:\\Users\\thota\\Downloads\\SpringBatch\\spring-batch\\OutputFiles\\studentsOutput.json
outputFile=C:\\Users\\thota\\Downloads\\SpringBatch\\spring-batch\\OutputFiles\\studentsOutput.xml

## Passing the currentItemCount and maxItemcount values to make database Migration dynamic
currentItemCount=0
maxItemCount=10000v

	//jakarta and jaxb dependency is required only if the java version is greater than 8
	// https://mvnrepository.com/artifact/jakarta.xml.bind/jakarta.xml.bind-api
	implementation 'jakarta.xml.bind:jakarta.xml.bind-api'
	// https://mvnrepository.com/artifact/org.glassfish.jaxb/jaxb-runtime
	implementation 'org.glassfish.jaxb:jaxb-runtime'

This is a required dependency... for xml reader..
// https://mvnrepository.com/artifact/org.springframework/spring-oxm
implementation 'org.springframework:spring-oxm:6.0.4'

Request Body: 
    [
        {
    "paramKey" : "test",
    "paramValue": "test123"
        }
    ]

Student table created..

CREATE TABLE `university`.`student` (
`id` INT NOT NULL,
`firstname` VARCHAR(45) ,
`lastname` VARCHAR(45) ,
`email` VARCHAR(45) ,
PRIMARY KEY (`id`));


----------------------------
SELECT * FROM university.student;

SELECT * FROM university.student;

INSERT INTO `university`.`student`
(`id`,
`firstname`,
`lastname`,
`email`)

VALUES(1,'John','Smith','johnsmith@gmail.com');

INSERT INTO `university`.`student`
(`id`,
`firstname`,
`lastname`,
`email`)
VALUES(2,'Ron','Weasley','ron@gmail.com');

INSERT INTO `university`.`student`
(`id`,
`firstname`,
`lastname`,
`email`)
VALUES(3,'Harry','Potter','potter@gmail.com');

INSERT INTO `university`.`student`
(`id`,
`firstname`,
`lastname`,
`email`)
VALUES(4,'Hermoine','Granger','granger@gmail.com');

INSERT INTO `university`.`student`
(`id`,
`firstname`,
`lastname`,
`email`)
VALUES(5,'George','Weasley','george.weasley@gmail.com');

INSERT INTO `university`.`student`
(`id`,
`firstname`,
`lastname`,
`email`)
VALUES(6,'Fred','Weasley','fred.weasley@gmail.com');


INSERT INTO `university`.`student`
(`id`,
`firstname`,
`lastname`,
`email`)
VALUES(7,'Ginny','Weasley','ginny.weasley@gmail.com');



INSERT INTO `university`.`student`
(`id`,
`firstname`,
`lastname`,
`email`)
VALUES(8,'Draco','Malfoy','malfoy.draco@gmail.com');

INSERT INTO `university`.`student`
(`id`,
`firstname`,
`lastname`,
`email`)
VALUES(9,'Neville','Longbottom','neville.longbottom@gmail.com');

INSERT INTO `university`.`student`
(`id`,
`firstname`,
`lastname`,
`email`)
VALUES(10,'Luna','Lovegood','luna.lovegood@gmail.com');


Errors faced...
Relying upon circular references is discouraged and they are prohibited by default. Update your application to remove the dependency cycle between beans. As a last resort, it may be possible to break the cycle automatically by setting spring.main.allow-circular-references to true.
Move the config to a different class....

Resolution:

Move the datasource beans to a separate class and try..

NOTE: If there are two datasources...
use jdbc-url instead of url.
spring.datasource.jdbc-url is correct 
instead of spring.datasource.url

Arrays.asList don't support add (or) remove operations...
https://rollbar.com/blog/fixing-unsupportedoperationexception-in-java/

## How to provide program arguments in Intellij
We cannot go into Terminal and give in the good old java Classname arg1 arg2 arg3

We'll have to edit the run configuration.

Step 1 : Take the Run menu
Step 2 : Select Edit Configurations
Step 3 : Fill the Program arguments field
