package com.example.springbatch.service;


import com.example.springbatch.model.Student;
import com.example.springbatch.model.StudentResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class StudentService {

   private List<StudentResponse> studentResponseList = null;
public StudentResponse getStudent(long id, String name)
{
    System.out.println("Id: "+ id + "Name: "+name);
    if(studentResponseList == null)
    {
       restCallToGetStudents();
    }
    if(studentResponseList != null  && !studentResponseList.isEmpty())
    {
        return studentResponseList.remove(0);
    }
    return null;
}

    public List<StudentResponse> restCallToGetStudents()
    {
        RestTemplate restTemplate = new RestTemplate();
        StudentResponse[] studentResponseArray = restTemplate.getForObject("http://localhost:8081/api/v1/students", StudentResponse[].class);

            studentResponseList = new ArrayList<>();
            for(StudentResponse studentResponse : studentResponseArray)
            {
                studentResponseList.add(studentResponse);
            }
        return studentResponseList;
    }



    public StudentResponse restCallToCreateStudent(Student studentRequest)
    {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<StudentResponse> studentResponseResponseEntity = restTemplate.postForEntity("http://localhost:8081/api/v1/createStudent", studentRequest, StudentResponse.class);
        StudentResponse studentResponse = studentResponseResponseEntity.getBody();
        System.out.println("createStudent endpoint returned response code:" + studentResponseResponseEntity.getStatusCode());
        return studentResponse;
    }


}
