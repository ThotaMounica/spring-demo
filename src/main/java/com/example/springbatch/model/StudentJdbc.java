package com.example.springbatch.model;

import lombok.Data;
import lombok.ToString;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "student")
@ToString
@Data
public class StudentJdbc {
    private long id;
    private String firstName;
    private String lastName;
    private String email;
}
