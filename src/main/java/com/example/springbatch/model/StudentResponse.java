package com.example.springbatch.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentResponse
{
    private long id;
    private String firstName;
    private String lastName;
    private String email;
}
