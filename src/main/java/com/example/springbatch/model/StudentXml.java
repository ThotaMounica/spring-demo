package com.example.springbatch.model;

import lombok.Data;
import lombok.ToString;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@ToString
@Data
@XmlRootElement(name = "student")
public class StudentXml {
    private long id;
    private String firstName;
    private String lastName;
    private String email;

    @XmlElement(name ="first_name")
    public String getFirstName()
    {
        return firstName;
    }
}
