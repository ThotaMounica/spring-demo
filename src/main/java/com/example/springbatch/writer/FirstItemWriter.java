package com.example.springbatch.writer;


import com.example.springbatch.model.Student;
import com.example.springbatch.model.StudentJdbc;
import com.example.springbatch.model.StudentResponse;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FirstItemWriter implements ItemWriter<Student> {
    @Override
    public void write(List<? extends Student> items) throws Exception {
        System.out.println("Inside FirstItemWriter...");
        System.out.println("List of Items:"+ items.size());

        items.stream().forEach(System.out::println);

    }
}
