package com.example.springbatch.listener;

import com.example.springbatch.model.Student;
import org.springframework.batch.core.SkipListener;
import org.springframework.batch.item.file.FlatFileParseException;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileWriter;
import java.util.Date;

@Component
public class SkipListenerImpl implements SkipListener<Student, Student>
{
    @Override
    public void onSkipInRead(Throwable th)
    {
        if (th instanceof FlatFileParseException)
        {
            createFile("C:\\Users\\thota\\Downloads\\SpringBatch\\spring-batch\\Second Job\\FirstChunkStep\\reader\\SkipInRead.txt"
                    , ((FlatFileParseException) th).getInput());
        }
    }

    @Override
    public void onSkipInProcess(Student item, Throwable t)
    {
        createFile("C:\\Users\\thota\\Downloads\\SpringBatch\\spring-batch\\Second Job\\FirstChunkStep\\processor\\SkipInProcess.txt"
                , item.toString());
    }

    @Override
    public void onSkipInWrite(Student item, Throwable t)
    {
        createFile("C:\\Users\\thota\\Downloads\\SpringBatch\\spring-batch\\Second Job\\FirstChunkStep\\writer\\SkipInWrite.txt"
                , item.toString());
    }

    public void createFile(String path, String data)
    {
        try (FileWriter fileWriter = new FileWriter(new File(path), true))
        {
            fileWriter.write(data + "," + new Date() +"\n");
        }
        catch (Exception e) {

        }
    }

}
