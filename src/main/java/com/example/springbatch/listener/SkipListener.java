package com.example.springbatch.listener;

import com.example.springbatch.model.Student;
import org.springframework.batch.core.annotation.OnSkipInProcess;
import org.springframework.batch.core.annotation.OnSkipInRead;
import org.springframework.batch.core.annotation.OnSkipInWrite;
import org.springframework.batch.item.file.FlatFileParseException;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

@Component
public class SkipListener {

    @OnSkipInRead
    public void skipInRead(Throwable th)
    {
        if (th instanceof FlatFileParseException)
        {
        createFile("C:\\Users\\thota\\Downloads\\SpringBatch\\spring-batch\\Second Job\\FirstChunkStep\\reader\\SkipInRead.txt", ((FlatFileParseException) th).getInput());
        }
    }

    @OnSkipInProcess
    public void skipInProcess(Student s, Throwable th)
    {
        createFile("C:\\Users\\thota\\Downloads\\SpringBatch\\spring-batch\\Second Job\\FirstChunkStep\\processor\\SkipInProcess.txt", s.toString());
    }

    @OnSkipInWrite
    public void skipInWrite(Student student, Throwable t)
    {
        createFile("C:\\Users\\thota\\Downloads\\SpringBatch\\spring-batch\\Second Job\\FirstChunkStep\\writer\\SkipInWrite.txt"
                , student.toString());
    }


    public void createFile(String path, String data)
    {
        try (FileWriter fileWriter = new FileWriter(new File(path), true))
        {
            fileWriter.write(data + "," + new Date() +"\n");
        }
        catch (Exception e) {

        }
    }
}
