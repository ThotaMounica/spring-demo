package com.example.springbatch.request;

import lombok.Data;

@Data
public class JobParamsRequest {

    private String paramKey;
    private String paramValue;

}
