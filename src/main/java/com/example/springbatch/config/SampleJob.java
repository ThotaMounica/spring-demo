package com.example.springbatch.config;


import com.example.springbatch.listener.FirstJobListener;
import com.example.springbatch.listener.FirstStepListener;
import com.example.springbatch.listener.SkipListener;
import com.example.springbatch.listener.SkipListenerImpl;
import com.example.springbatch.model.Student;
import com.example.springbatch.model.StudentJdbc;
import com.example.springbatch.model.StudentResponse;
import com.example.springbatch.model.StudentXml;
import com.example.springbatch.processor.FirstItemProcessor;
import com.example.springbatch.processor.StudentItemProcessor;
import com.example.springbatch.processor.SubjectsItemProcessor;
import com.example.springbatch.reader.FirstItemReader;
import com.example.springbatch.service.SecondTasklet;
//import com.example.springbatch.service.StudentService;
import com.example.springbatch.service.StudentService;
import com.example.springbatch.writer.FirstItemWriter;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.skip.AlwaysSkipItemSkipPolicy;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.adapter.ItemReaderAdapter;
import org.springframework.batch.item.adapter.ItemWriterAdapter;
import org.springframework.batch.item.database.*;
import org.springframework.batch.item.file.*;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.json.*;
import org.springframework.batch.item.xml.StaxEventItemReader;
import org.springframework.batch.item.xml.StaxEventItemWriter;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.scheduling.concurrent.ConcurrentTaskExecutor;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import javax.sql.RowSetInternal;
import javax.sql.rowset.WebRowSet;
import javax.sql.rowset.spi.XmlReader;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@Configuration
@EnableBatchProcessing
public class SampleJob {

    @Autowired
    private JobBuilderFactory jobBuilder;


    @Autowired
    private StepBuilderFactory stepBuilder;

    @Autowired
    private SecondTasklet secondTasklet;

    @Autowired
    private FirstJobListener firstJobListener;

    @Autowired
    private FirstStepListener firstStepListener;

    @Autowired
    private FirstItemReader firstItemReader;

    @Autowired
    private FirstItemProcessor firstItemProcessor;

    @Autowired
    private StudentItemProcessor studentItemProcessor;

    @Autowired
    private SubjectsItemProcessor subjectsItemProcessor;

    @Autowired
    private FirstItemWriter firstItemWriter;

    @Autowired
    private StudentService studentService;

    @Autowired
    private SkipListener skipListener;

    @Autowired
    private SkipListenerImpl skipListenerImpl;

    @Qualifier("universityDataSource")
    @Autowired
    private DataSource universityDataSource;

    @Qualifier("postgresDataSource")
    @Autowired
    private DataSource postgresDataSource;

    @Autowired
    @Qualifier("postgresqlEntityManagerFactory")
    private EntityManagerFactory postgresqlEntityManagerFactory;

    @Autowired
    @Qualifier("mysqlEntityManagerFactory")
    private  EntityManagerFactory mysqlEntityManagerFactory;


    @Autowired
    private JpaTransactionManager jpaTransactionManager;

    @Bean
    public Job firstJob() {
        return jobBuilder
                .get("First Job")
                .incrementer(new RunIdIncrementer())
                .start(firstStep())
                .next(secondStep())
                .listener(firstJobListener)
                .build();
    }

    private Step firstStep()
    {
        return stepBuilder
                .get("firstStep")
                .tasklet(firstTask())
                .listener(firstStepListener)
                .build();
    }

    private Step secondStep()
    {
        return stepBuilder
                .get("secondStep")
                .tasklet(secondTasklet)
                .build();
    }

    private Tasklet firstTask() {
        return new Tasklet() {
            @Override
            public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
                System.out.println("This is first tasklet step...");
                System.out.println("JobExecutionContext:"+ chunkContext.getStepContext().getJobExecutionContext());
                System.out.println("StepExecutionContext:"+ chunkContext.getStepContext().getStepExecutionContext());
                return RepeatStatus.FINISHED;
            }
        };
    }

    @Bean
    public Job secondJob()
    {
        return jobBuilder
                .get("Second Job")
                .incrementer(new RunIdIncrementer())
                .start(firstChunkStep())
                .build();
    }

    private Step firstChunkStep()
    {
        return stepBuilder.get("FirstChunkStep")
                .<com.example.springbatch.postgres.entity.SubjectsLearning, com.example.springbatch.mysql.entity.SubjectsLearning>chunk(200)
//                .reader(itemReaderAdapter())
//                .reader(jdbcCursorItemReader())
//                .reader(staxEventItemReader(null))
//                .reader(jsonItemReader(null))
//                .reader(flatFileItemReader(null))
//                .processor(firstItemProcessor)
//                .writer(firstItemWriter)
//                .writer(flatFileItemWriter(null))
//                .writer(jsonFileItemWriter(null))
                .reader(jpaCursorItemReaderSubjects(null, null))
                .processor(subjectsItemProcessor)
                .writer(jpaItemWriterSubjects())
                .faultTolerant()
//                .skip(FlatFileParseException.class)
                .skip(Throwable.class)
                .skipLimit(100)
//                .skipLimit(Integer.MAX_VALUE)
//                .skipPolicy(new AlwaysSkipItemSkipPolicy())
//                .listener(skipListener)
                .retry(Throwable.class)
                .retryLimit(5)
                .listener(skipListenerImpl)
                .transactionManager(jpaTransactionManager)
//                .writer(staxEventItemWriter(null))
//                .writer(jdbcBatchItemWriter())
//                .writer(itemWriterAdapter())
//                .writer(preparedStatementJdbcBatchItemWriter())
                .build();
    }

    @Bean
    @StepScope
    public FlatFileItemReader<Student> flatFileItemReader(@Value("#{jobParameters['inputFile']}") FileSystemResource inputFileSystemResource)
    {
        FlatFileItemReader<Student> flatFileItemReader =
                new FlatFileItemReader<>();
        flatFileItemReader.setResource(inputFileSystemResource);

        DelimitedLineTokenizer delimitedLineTokenizer = new DelimitedLineTokenizer();
//        delimitedLineTokenizer.setDelimiter("|");
        delimitedLineTokenizer.setNames("ID","First Name","Last Name","Email");

        BeanWrapperFieldSetMapper<Student> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(Student.class);
        DefaultLineMapper<Student> lineMapper = new DefaultLineMapper<>();

        lineMapper.setLineTokenizer(delimitedLineTokenizer);
        lineMapper.setFieldSetMapper(fieldSetMapper);
        flatFileItemReader.setLineMapper(lineMapper);
        flatFileItemReader.setLinesToSkip(1);

        return flatFileItemReader;
    }

    @Bean
    @StepScope
    public JsonItemReader<Student> jsonItemReader(@Value("#{jobParameters['inputFile']}") FileSystemResource inputFileSystemResource)
    {
        System.out.println("Json ItemReader....");
        JsonItemReader<Student> jsonItemReader = new JsonItemReader<>();
        jsonItemReader.setResource(inputFileSystemResource);
        jsonItemReader.setMaxItemCount(8);
        jsonItemReader.setCurrentItemCount(2);
        jsonItemReader.setJsonObjectReader(new JacksonJsonObjectReader<>(Student.class));
        return jsonItemReader;
    }

    @Bean
    @StepScope
    public StaxEventItemReader<StudentXml> staxEventItemReader(@Value("#{jobParameters['inputFile']}") FileSystemResource inputFileSystemResource)
    {
        System.out.println("Xml ItemReader....");

        StaxEventItemReader<StudentXml> staxEventItemReader = new StaxEventItemReader<>();
        staxEventItemReader.setResource(inputFileSystemResource);
        staxEventItemReader.setFragmentRootElementName("student");

        Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
        jaxb2Marshaller.setClassesToBeBound(StudentXml.class);

        staxEventItemReader.setUnmarshaller(jaxb2Marshaller);

        return staxEventItemReader;
    }


    public JdbcCursorItemReader<StudentJdbc> jdbcCursorItemReader()
    {

        System.out.println("Inside JDBC ItemReader....");
        JdbcCursorItemReader<StudentJdbc> jdbcCursorItemReader = new JdbcCursorItemReader<>();

        jdbcCursorItemReader.setDataSource(universityDataSource);
        jdbcCursorItemReader.setSql("select id, firstname as firstName, lastname as lastName, email from university.student");
//        jdbcCursorItemReader.setCurrentItemCount(2);
//        jdbcCursorItemReader.setMaxItemCount(8);
        BeanPropertyRowMapper<StudentJdbc> studentJdbcBeanPropertyRowMapper = new BeanPropertyRowMapper<StudentJdbc>();
        studentJdbcBeanPropertyRowMapper.setMappedClass(StudentJdbc.class);

        jdbcCursorItemReader.setRowMapper(studentJdbcBeanPropertyRowMapper);

        return jdbcCursorItemReader;
    }

//public ItemReaderAdapter<StudentResponse> itemReaderAdapter()
//{
//    ItemReaderAdapter<StudentResponse> itemReaderAdapter = new ItemReaderAdapter<>();
//
//    itemReaderAdapter.setTargetObject(studentService);
//    itemReaderAdapter.setTargetMethod("getStudent");
//    itemReaderAdapter.setArguments(new Object[] {1L, "Test Argument..."});
//
//    return itemReaderAdapter;
//}


    @Bean
    @StepScope
    public FlatFileItemWriter<StudentJdbc> flatFileItemWriter(@Value("#{jobParameters['outputFile']}") FileSystemResource outputFileSystemResource)
    {
        FlatFileItemWriter<StudentJdbc> flatFileItemWriter = new FlatFileItemWriter<>();
        flatFileItemWriter.setResource(outputFileSystemResource);
        FlatFileHeaderCallback fileHeaderCallback = new FlatFileHeaderCallback() {
            @Override
            public void writeHeader(Writer writer) throws IOException {
                writer.write("Id,First Name,Last Name,Email");
//                writer.write("Id|First Name|Last Name|Email");
            }
        };

        flatFileItemWriter.setHeaderCallback(fileHeaderCallback);
        DelimitedLineAggregator<StudentJdbc> lineAggregator = new DelimitedLineAggregator<>();
        BeanWrapperFieldExtractor<StudentJdbc> fieldExtractor = new BeanWrapperFieldExtractor<>();
        fieldExtractor.setNames(new String[]{"id", "firstName", "lastName", "email"});
        lineAggregator.setFieldExtractor(fieldExtractor);
//        lineAggregator.setDelimiter("|");
        flatFileItemWriter.setLineAggregator(lineAggregator);
        flatFileItemWriter.setFooterCallback(new FlatFileFooterCallback() {
            @Override
            public void writeFooter(Writer writer) throws IOException {
                writer.write("Created @: "+ new Date());
            }
        });
        return flatFileItemWriter;
    }


    @Bean
    @StepScope
    public JsonFileItemWriter<Student> jsonFileItemWriter(@Value("#{jobParameters['outputFile']}") FileSystemResource outputFileSystemResource)
    {
        JsonFileItemWriter<Student> jsonFileItemWriter = new JsonFileItemWriter<>(outputFileSystemResource , new JacksonJsonObjectMarshaller<Student>())
        {
            @Override
            public String doWrite(List<? extends Student> items) {
               for(Student s: items)
               {
                   if(s.getId() ==3)
                   {
                       System.out.println("Inside Item jsonFileItemWriter...");
                       throw new NullPointerException();
                   }
               }
                return super.doWrite(items);
            }
        };
        return jsonFileItemWriter;
    }


    @Bean
    @StepScope
    public StaxEventItemWriter<StudentJdbc> staxEventItemWriter(@Value("#{jobParameters['outputFile']}") FileSystemResource outputFileSystemResource)
    {
        StaxEventItemWriter<StudentJdbc> staxEventItemWriter = new StaxEventItemWriter<>();
        staxEventItemWriter.setResource(outputFileSystemResource);
        staxEventItemWriter.setRootTagName("Students");
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setClassesToBeBound(StudentJdbc.class);
        staxEventItemWriter.setMarshaller(marshaller);

        return staxEventItemWriter;
    }


    @StepScope
    @Bean
    public JdbcBatchItemWriter<Student> jdbcBatchItemWriter()
    {
        JdbcBatchItemWriter<Student> jdbcBatchItemWriter = new JdbcBatchItemWriter<>();
        jdbcBatchItemWriter.setDataSource(universityDataSource);
        jdbcBatchItemWriter.setSql("insert into university.student(id, firstName, lastName, email) " +
                "values (:id, :firstName, :lastName, :email)");
        jdbcBatchItemWriter.setItemSqlParameterSourceProvider(
                new BeanPropertyItemSqlParameterSourceProvider<Student>());

        return jdbcBatchItemWriter;

    }

    @StepScope
    @Bean
    public JdbcBatchItemWriter<Student> preparedStatementJdbcBatchItemWriter()
    {
        JdbcBatchItemWriter<Student> jdbcBatchItemWriter = new JdbcBatchItemWriter<>();
        jdbcBatchItemWriter.setDataSource(universityDataSource);
        jdbcBatchItemWriter.setSql("insert into university.student(id, firstName, lastName, email) " +
                "values (?, ?, ?, ?)");
        jdbcBatchItemWriter.setItemPreparedStatementSetter(new ItemPreparedStatementSetter<Student>() {
            @Override
            public void setValues(Student item, PreparedStatement ps) throws SQLException {
                ps.setLong(1, item.getId());
                ps.setString(2, item.getFirstName());
                ps.setString(3, item.getLastName());
                ps.setString(4, item.getEmail());
            }
        });
        return jdbcBatchItemWriter;
    }

//    public ItemWriterAdapter<Student> itemWriterAdapter()
//    {
//        ItemWriterAdapter<Student> itemWriterAdapter = new ItemWriterAdapter<>();
//
//        itemWriterAdapter.setTargetObject(studentService);
//        itemWriterAdapter.setTargetMethod("restCallToCreateStudent");
//
//        return itemWriterAdapter;
//    }

    public JpaCursorItemReader<com.example.springbatch.postgres.entity.Student>  jpaCursorItemReader()
    {
        JpaCursorItemReader<com.example.springbatch.postgres.entity.Student> jpaCursorItemReader
                = new JpaCursorItemReader<>();

        jpaCursorItemReader.setEntityManagerFactory(postgresqlEntityManagerFactory);
        jpaCursorItemReader.setQueryString("From Student");

        return jpaCursorItemReader;
    }

    @Bean
    @StepScope
    public JpaCursorItemReader<com.example.springbatch.postgres.entity.SubjectsLearning>  jpaCursorItemReaderSubjects(@Value("#{jobParameters['currentItemCount']}") Integer currentItemCount
    , @Value("#{jobParameters['maxItemCount']}") Integer maxItemCount)

    {
        JpaCursorItemReader<com.example.springbatch.postgres.entity.SubjectsLearning> jpaCursorItemReader
                = new JpaCursorItemReader<>();

        jpaCursorItemReader.setEntityManagerFactory(postgresqlEntityManagerFactory);
        jpaCursorItemReader.setQueryString("From SubjectsLearning");
        jpaCursorItemReader.setCurrentItemCount(currentItemCount);
        jpaCursorItemReader.setMaxItemCount(maxItemCount);

        return jpaCursorItemReader;
    }

    public JpaItemWriter<com.example.springbatch.mysql.entity.Student> jpaItemWriter()
    {
        JpaItemWriter<com.example.springbatch.mysql.entity.Student> jpaItemWriter = new JpaItemWriter<>();
        jpaItemWriter.setEntityManagerFactory(mysqlEntityManagerFactory);
        return jpaItemWriter;
    }

    public JpaItemWriter<com.example.springbatch.mysql.entity.SubjectsLearning> jpaItemWriterSubjects()
    {
        JpaItemWriter<com.example.springbatch.mysql.entity.SubjectsLearning> jpaItemWriter = new JpaItemWriter<>();
        jpaItemWriter.setEntityManagerFactory(mysqlEntityManagerFactory);
        return jpaItemWriter;
    }

}
