package com.example.springbatch.postgres.entity;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "subjects_learning")
@Data
public class SubjectsLearning
{
    @Id
    private Long id;
    @Column(name = "sub_name")
    private String subName;
    @Column(name = "student_id")
    private Long studentId;
    @Column(name = "marks_obtained")
    private Long marksObtained;
}
