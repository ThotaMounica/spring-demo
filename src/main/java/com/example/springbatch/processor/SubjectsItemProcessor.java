package com.example.springbatch.processor;



import com.example.springbatch.postgres.entity.SubjectsLearning;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;


@Component
public class SubjectsItemProcessor implements ItemProcessor<SubjectsLearning, com.example.springbatch.mysql.entity.SubjectsLearning> {

    @Override
    public com.example.springbatch.mysql.entity.SubjectsLearning process(SubjectsLearning item) throws Exception
    {
        System.out.println("Item Id: "+item.getId());
        com.example.springbatch.mysql.entity.SubjectsLearning subjectsLearningSql = new com.example.springbatch.mysql.entity.SubjectsLearning();
        subjectsLearningSql.setId(item.getId());
        subjectsLearningSql.setStudentId(item.getStudentId());
        subjectsLearningSql.setMarksObtained(item.getMarksObtained());
        subjectsLearningSql.setSubName(item.getSubName());
        return subjectsLearningSql;
    }
}
