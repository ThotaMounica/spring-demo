package com.example.springbatch.processor;


import com.example.springbatch.model.Student;
import com.example.springbatch.model.StudentJdbc;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
public class FirstItemProcessor implements ItemProcessor<Student, Student> {

    @Override
    public Student process(Student item) throws Exception
    {
        if(item.getId() == 6)
        {
            System.out.println("Inside Item Processor...");
            throw new NullPointerException();
        }
        Student student = new Student();
        student.setId(item.getId());
        student.setFirstName(item.getFirstName());
        student.setLastName(item.getLastName());
        student.setEmail(item.getEmail());
        return student;
    }
}
