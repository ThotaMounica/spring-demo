package com.example.springbatch.processor;


import com.example.springbatch.postgres.entity.Student;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;


@Component
public class StudentItemProcessor implements ItemProcessor<Student, com.example.springbatch.mysql.entity.Student> {
    @Override
    public com.example.springbatch.mysql.entity.Student process(Student item) throws Exception {

        System.out.println("Id: "+item.getId());
        com.example.springbatch.mysql.entity.Student mySqlStudent = new com.example.springbatch.mysql.entity.Student();
        mySqlStudent.setId(item.getId());
        mySqlStudent.setFirstName(item.getFirstName());
        mySqlStudent.setLastName(item.getLastName());
        mySqlStudent.setEmail(item.getEmail());
        mySqlStudent.setDeptId(item.getDeptId());
        mySqlStudent.setIsActive(item.getIsActive() != null? Boolean.valueOf(item.getIsActive()) : false);
        return mySqlStudent;
    }
}
