/*package com.example.springbatch.controller;


import com.example.springbatch.request.JobParamsRequest;
import com.example.springbatch.service.JobService;
import org.springframework.batch.core.launch.JobExecutionNotRunningException;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.batch.core.launch.NoSuchJobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/job")
public class JobController {

    @Autowired
    JobService jobService;

    @Autowired
    JobOperator jobOperator;

    @GetMapping("/start/{jobName}")
    public String startJob(@PathVariable String jobName, @RequestBody List<JobParamsRequest> jobParamsRequestList){
        jobService.startJob(jobName, jobParamsRequestList);

        return jobName + " Job Started....";
    }

    @GetMapping("/stop/{jobExecutionId}")
    public String stopJob(@PathVariable long jobExecutionId)
    {
        try {
            jobOperator.stop(jobExecutionId);
        } catch (Exception e) {
           e.printStackTrace();
        }
        return "Job Stopped..";
    }


}
*/